﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;

namespace xamarin_1
{
    [Activity(Label = "Test APP", Theme = "@android:style/Theme.Material.Light.NoActionBar", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            Button button = FindViewById<Button>(Resource.Id.btnFirst);
            button.Click += ShowAlert;
        }

        protected void ShowAlert(object obj, EventArgs args)
        {
            AlertDialog.Builder alertDiag = new AlertDialog.Builder(this);
            alertDiag.SetTitle("Hello! Hello!! Hello!!!");
            alertDiag.SetMessage("You Clicked this button");
            Dialog diag = alertDiag.Create();
            diag.Show();
        }
    }
}

